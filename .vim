:set tags=./tags
:e basins/server/server.py
:rightbelow vsp basins/client/client.py
:tabnew basins/server/task_manager.py
:tabnew basins/server/t_server.py
:rightbelow vsp basins/client/worker.py
:tabnew basins/common/algorithm.py

:tabnew .vim
:rightbelow vsp .screenrc

:tabn
