"""Basins

See:
https://bitbucket.org/magnofamily/basins/overview
"""

from setuptools import setup
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, "README.md"), encoding="utf-8") as f:
    long_description = f.read()

setup(
    name="basins",
    version="0.1.0",
    description="Basins project",
    long_description=long_description,
    url="https://bitbucket.org/magnofamily/basins",
    author="The Magno Family",
    author_email="goncalo.magno@gmail.com",
    license="MIT",
    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        "Development Status :: 1 - Planning",
        "Intended Audience :: Education",
        "Topic :: Scientific/Engineering :: Visualization",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 2.7",
    ],
    # What does your project relate to?
    keywords="newton basins",

    # You can just specify the packages manually here if your project is
    # simple. Or you can use find_packages().
    # packages=find_packages(exclude=["contrib", "docs", "tests"]),

    # Alternatively, if you want to distribute just a my_module.py, uncomment
    # this:
    #   py_modules=["my_module"],

    # List run-time dependencies here.  These will be installed by pip when
    # your project is installed. For an analysis of "install_requires" vs pip"s
    # requirements files see:
    # https://packaging.python.org/en/latest/requirements.html
    install_requires=["apipkg>=1.4",
                      "decorator>=4.0.9",
                      "execnet>=1.4.1",
#                       "ipython",
#                       "ipython-genutils",
                      "path.py>=8.1.2",
                      "pep8==1.7.0",
                      "peppercorn>=0.5",
                      "pexpect>=4.0.1",
                      "pickleshare>=0.6",
                      "pkginfo>=1.2.1",
                      "ptyprocess>=0.5.1",
                      "py>=1.4.31",
                      "pytest>=2.8.7",
                      "pytest-cache>=1.0",
                      "requests>=2.9.1",
                      "requests-toolbelt>=0.6.0",
                      "simplegeneric>=0.8.1",
                      "traitlets>=4.1.0",
                      "twine>=1.6.5",
                      "wheel>=0.24.0", ],


    # List additional groups of dependencies here (e.g. development
    # dependencies). You can install these using the following syntax,
    # for example:
    # $ pip install -e .[dev,test]
#     extras_require={
#         "dev": ["check-manifest"],
#         "test": ["coverage"],
#     },

    # If there are data files included in your packages that need to be
    # installed, specify them here.  If using Python 2.6 or less, then these
    # have to be included in MANIFEST.in as well.
#     package_data={
#         "sample": ["package_data.dat"],
#     },

    # Although "package_data" is the preferred approach, in some case you may
    # need to place data files outside of your packages. See:
    # http://docs.python.org/3.4/distutils/setupscript.html#installing-additional-files # noqa
    # In this case, "data_file" will be installed into "<sys.prefix>/my_data"
#     data_files=[("my_data", ["data/data_file"])],

    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # pip to create the appropriate form of executable for the target platform.
    entry_points={
        "console_scripts": [
            "basinsserv=basins.server:main",
            "basinscli=basins.server:main",
        ],
    },
)
