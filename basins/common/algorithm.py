import math
import numpy
import colorsys
from PIL import Image
import pprint


class Newton():

    def split_image(self, div,
                    img_width, img_height,
                    half_real_size, half_imag_size,
                    real_offset, imag_offset):
        """
        Slices an image in chunks of 1 (no slicing), 4 (2x2), 16 (4x4), etc,
        only multiples of 4 are allowed. The output image is not necessarily
        the size requested, will be the closest possible.
        :param img_width:
        :param img_height:
        :param half_real_size:
        :param half_imag_size:
        :param real_offset:
        :param imag_offset:
        :return: a dict with the following structure (e.g of 2x2 slicing):
            {(0, 0): { "img_width":, "img_height":, "half_real_size":, "half_imag_size":, "real_offset":, "imag_offset": },
             (0, 1): { "img_width":, "img_height":, "half_real_size":, "half_imag_size":, "real_offset":, "imag_offset":),
             (1, 0): { "img_width":, "img_height":, "half_real_size":, "half_imag_size":, "real_offset":, "imag_offset":),
             (1, 1): { "img_width":, "img_height":, "half_real_size":, "half_imag_size":, "real_offset":, "imag_offset":)}
        """
        default_return = {(0, 0): {"img_width": img_width, "img_height": img_width,
                                   "half_real_size": half_real_size, "half_imag_size": half_imag_size,
                                   "real_offset": real_offset, "imag_offset": imag_offset}}
        if div == 1 or div % 4 != 0:
            return default_return

        matrix_size = int(math.sqrt(div))
        new_img_width = img_width / matrix_size
        new_img_height = img_height / matrix_size
        if new_img_width <= 2 or new_img_height <= 2:
            # chunks are too small, try smaller div
            return default_return
        # setting the image size to the closest odd size
        if not (new_img_width % 2):
            new_img_width += 1
        if not (new_img_height % 2):
            new_img_height += 1

        new_half_real_size = half_real_size / matrix_size
        new_half_imag_size = half_imag_size / matrix_size

        ret = {}
        # starting on the upper left-hand corner
        for i in range(matrix_size-1, -1, -1):
            for j in range(matrix_size):
                key = (i, j)
                new_real_offset = real_offset - half_real_size + (j*2.0 + 1.0)*new_half_real_size
                new_imag_offset = imag_offset - half_imag_size + ((matrix_size-1-i)*2.0 + 1.0)*new_half_imag_size
                value = {"img_width": new_img_width, "img_height": new_img_height,
                         "half_real_size": new_half_real_size, "half_imag_size": new_half_imag_size,
                         "real_offset": new_real_offset, "imag_offset": new_imag_offset}
                ret.update({key: value})
        return ret

    def merge_images(self, ):
        pass

    def gen_image(self, equ_coefs, max_iter_to_root,
                  img_width, img_height,
                  half_real_size, half_imag_size,
                  real_offset, imag_offset):
        """
        Generates a Newton fractal in raw format:
        [(R, G, B), (R, G, B), ..., (R, G, B)]
        :param equ_coefs: polynomial equation coefficients:
        aN*x^N + ... + a2*x^2 + a1*x + a0 => [aN, ..., a2, a1, a0]
        :param max_iter_to_root: maximum no. of iterations for the algorithm to
        converge to the root value.
        :param img_width: output image width in pixels. Should be odd,
        otherwise the closest odd number is used.
        :param img_height: output image heigh in pixels. Should be odd,
        otherwise the closest odd number is used.
        :param half_real_size: positive boundary in the complex plane for the real
        component
        :param half_imag_size: positive boundary in the complex plane for the imag
        component
        :param real_offset: real part of the center of the image in the complex
        plane coordinates
        :param imag_offset: imag part of the center of the image in the complex
        plane coordinates
        """
        # setting the image size to the closest odd size
        if not (img_width % 2):
            img_width += 1
        if not (img_height % 2):
            img_height += 1
    
        roots = numpy.roots(equ_coefs)
        temproots = roots
        # iterate over roots and check if there are duplicates
        for o1 in range(len(roots)):
            for o2 in range(len(roots)):
                is_duplicate_real = True
                try:
                    numpy.testing.assert_allclose(roots[o2].real,
                                                  roots[o1].real)
                except:
                    is_duplicate_real = False
                is_duplicate_img = True
                try:
                    numpy.testing.assert_allclose(roots[o2].img,
                                                  roots[o1].img)
                except:
                    is_duplicate_img = False
                if is_duplicate_real and is_duplicate_img and o1 != o2:
                    temproots[o1] = float("nan")
        # remove duplicate roots
        roots = [r for r in temproots if not math.isnan(r.real)]
        # setting roots colors in HSV color space
        roots_color_h = [0.0]*len(roots)
        roots_color_s = [0.0]*len(roots)
        roots_color_v = [0.0]*len(roots)
        for r in range(len(roots)):
            roots_color_h[r] = float(float(r)/float(len(roots)))
            roots_color_s[r] = 1.0
            roots_color_v[r] = 0.0
    
        # create buffer to hold the image RGB data
        image_raw = []
        half_img_height = (img_height-1)/2  # in pixels
        half_img_width = (img_width-1)/2  # in pixels
        # start on top left-hand corner pixel
        for h in range(half_img_height + 1, -half_img_height, -1):
            for w in range(-half_img_width, half_img_width + 1):
                initial_condition = \
                    1.0*w/half_img_width*half_real_size + real_offset + \
                    1.0*h/half_img_height*half_imag_size*1.0j + imag_offset*1.0j
                z = initial_condition
                # newton-raphson method
                for it in range(max_iter_to_root):
                    # f(z)
                    num = numpy.polyval(equ_coefs, z)
                    # f"(z)
                    den = numpy.polyval(numpy.polyder(
                                            numpy.poly1d(equ_coefs)), z)
                    if den != 0.0:
                        z = z - num/den
                    num = numpy.polyval(equ_coefs, z)
                    if abs(num) < 1e-7:
                        break
    
                # find the nearest root
                root_index = roots.index(min(roots, key=lambda x: abs(x-z)))
                hh = roots_color_h[root_index]
                ss = roots_color_s[root_index]
                vv = 1.0 - float(it)/float(max_iter_to_root)
                rr, gg, bb = colorsys.hsv_to_rgb(hh, ss, vv)
                image_raw.append((int(rr*255.0), int(gg*255.0), int(bb*255.0)))
        metadata = {"img_width": img_width, "img_height": img_height,
                    "half_real_size": half_real_size, "half_imag_size": half_imag_size,
                    "real_offset": real_offset, "imag_offset": imag_offset}
        return (metadata, image_raw)


def main():
#     image_width_px = 51
#     image_height_px = 51
#     ratio = 1.0*image_width_px/image_height_px

    n = Newton()
    splits = n.split_image(div=4,
                           img_width=22, img_height=22,
                           half_real_size=1.0, half_imag_size=1.0,
                           real_offset=0.0, imag_offset=0.0)
    pprint.pprint(splits)
    (meta00, image_raw00) = n.gen_image(equ_coefs=[1, 0, 0, 0, 0, -1],
                            max_iter_to_root=30,
                            **splits[(0, 0)])
    (meta01, image_raw01) = n.gen_image(equ_coefs=[1, 0, 0, 0, 0, -1],
                              max_iter_to_root=30,
                              **splits[(0, 1)])
    (meta10, image_raw10) = n.gen_image(equ_coefs=[1, 0, 0, 0, 0, -1],
                              max_iter_to_root=30,
                              **splits[(1, 0)])
    (meta11, image_raw11) = n.gen_image(equ_coefs=[1, 0, 0, 0, 0, -1],
                              max_iter_to_root=30,
                              **splits[(1, 1)])
    outimg00 = Image.new("RGB", (11, 11))
    outimg00.putdata(image_raw00)
    outimg00.save("image00.png")

    outimg01 = Image.new("RGB", (11, 11))
    outimg01.putdata(image_raw01)
    outimg01.save("image01.png")

    outimg10 = Image.new("RGB", (11, 11))
    outimg10.putdata(image_raw10)
    outimg10.save("image10.png")

    outimg11 = Image.new("RGB", (11, 11))
    outimg11.putdata(image_raw11)
    outimg11.save("image11.png")

    outimg = Image.new("RGB", (22, 22))
    outimg.paste(outimg00, (0, 0))
    outimg.paste(outimg01, (11, 0))
    outimg.paste(outimg10, (0, 11))
    outimg.paste(outimg11, (11, 11))
    outimg.save("image_final.png")


#     image_raw = n.gen_image(image_width_px, image_height_px,
#                             1.0, 1.0/ratio,
#                             0.0, 0.0,
#                             [1, 0, 0, 0, 0, -1],
#                             30)
# 
#     # create output image in png format for visualization purposes
#     outimg = Image.new("RGB", (image_width_px, image_height_px))
#     outimg.putdata(image_raw00)
#     outimg.save("image.png")


if __name__ == "__main__":
    main()
