import xmlrpclib
import base64
import logging
from basins.common.algorithm import Newton
from time import sleep


logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',)
                    #datefmt='%m-%d %H:%M')
logger = logging.getLogger(__name__)


# Task (dict):
#    """ A Task represents a piece of work that can be executed by a client.
#    """
#       - id              # unique uuid
#       - aid             # Assignment uuid (if task is associated with an assignment)
#       - description     # private data describing the task
#       - result          # private data contaning the result
#
# `description`:
#   - equ_coefs
#   - max_iter_to_root
#   - img_width
#   - img_height
#   - half_real_size
#   - half_imag_size
#   - real_offset
#   - imag_offset

if __name__ == "__main__":
    wait_time = 60
    s = xmlrpclib.ServerProxy('http://127.0.0.1:8000')
    n = Newton()
    
    while True:
        logging.debug("Requesting a task")
        task = s.request_task()

        if task:
            logging.debug("Task retrieved: %s", task)
            logging.debug("Computing task")
            task["result"] = n.gen_image(**task["description"])
            logging.debug("Submit the result to the server")
            s.submit_resolved_task(task)
            logging.debug("Resolved task submitted")


        else:
            logging.debug("No tasks retrieved. Waiting %s secs", wait_time)
            sleep(wait_time)





    

    #encoded_result = "encoded result"
    #task["result"] = encoded_result
    #s.submit_resolved_task(task)




    #result = base64.b64decode(encoded_result)
    #print "Task given:", task_id, task

