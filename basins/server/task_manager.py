from uuid import uuid1
from threading import Lock
import pprint
#from basins.common.algorithm import Newton

import logging


logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',)
                    #datefmt='%m-%d %H:%M')
logger = logging.getLogger(__name__)


# Task (dict):
#    """ A Task represents a piece of work that can be executed by a client.
#    """
#       - id              # unique uuid
#       - aid             # Assignment uuid (if task is associated with an assignment)
#       - description     # private data describing the task
#       - result          # private data contaning the result


class TaskManager:
    """ The TaskManager provides a set of calls easy to use to interact with
    the list of Assignment's and their corresponding Task's.
    tasks_available, tasks_taken, tasks_resolved:
    {
        "assignment_0_uuid":
            {
                "task_0_uuid": Task,
                "task_1_uuid": Task,
                ...
            },
        "assignment_1_uuid":
            {
                "task_0_uuid": Task,
                "task_1_uuid": Task,
                ...
            },
            ...
    }
    """
    def __init__(self):
        self.lock_available_1 = Lock()

        # Initialising with a debug task
        self.tasks_available = {
                                    "assignX":
                                    {
                                        "taskX":
                                        {
                                            "id": "taskX",
                                            "aid": "assignX",
                                            "description":
                                            {
                                                "equ_coefs": [1, 0, 0, 0, 0, -1],
                                                "max_iter_to_root": 30,
                                                "img_width": 23,
                                                "img_height": 23,
                                                "half_real_size": 1.0,
                                                "half_imag_size": 1.0,
                                                "real_offset": 0.0,
                                                "imag_offset": 0.0,
                                            },
                                            "result": ()
                                        }
                                    }
                               }

        self.lock_taken_2 = Lock()
        self.tasks_taken = {}
        
        self.lock_resolved_3 = Lock()
        self.tasks_resolved = {}

        #self.newton = Newton()

    def request_task(self, assignment=None):
        """ Returns any available Task.
        """
        with self.lock_available_1:
            #logging.debug("tasks_available before retrieval: %s", self.tasks_available)
            available_task = ""

            if self.tasks_available:
                logging.debug("Retrieving an available task")
                available_assignment_id = self.tasks_available.items()[0][0]
                available_task = self.tasks_available.items()[0][1].popitem()[1]
                #logging.debug("Retrieved available_task: %s", available_task)

                # Remove assignment if retrieved the last element of the list
                if not self.tasks_available[available_task["aid"]]:
                    self.tasks_available.pop(available_task["aid"])
                #logging.debug("tasks_available after retrieval: %s", self.tasks_available)

                with self.lock_taken_2:
                    #logging.debug("tasks_taken before update: %s", self.tasks_taken)
                    if available_task["aid"] not in  self.tasks_taken:
                        self.tasks_taken[available_task["aid"]] = {}
                    self.tasks_taken[available_task["aid"]][available_task["id"]] = available_task
                    #logging.debug("tasks_taken after update: %s", self.tasks_taken)
            else:
                logging.debug("No tasks available")

        return available_task


    def submit_resolved_task(self, task):
        #logging.debug("Receiving a resolved task: %s", task)
        with self.lock_taken_2:
            #logging.debug("tasks_taken before update: %s", self.tasks_taken)
            self.tasks_taken[task["aid"]].pop(task["id"])

            # Remove assignment if retrieved the last element of the list
            if not self.tasks_taken[task["aid"]]:
                self.tasks_taken.pop(task["aid"])
            #logging.debug("tasks_taken after update: %s", self.tasks_taken)

            with self.lock_resolved_3:
                #logging.debug("tasks_resolved before update: %s", self.tasks_resolved)
                if task["aid"] not in self.tasks_resolved:
                    self.tasks_resolved[task["aid"]] = {}
                self.tasks_resolved[task["aid"]][task["id"]] = task
                #logging.debug("tasks_resolved after update: %s", self.tasks_resolved)


    def add_tasks(self, aids):
        logging.debug("Adding a new set of tasks: %s", pprint.pformat(aids))
        with self.lock_available_1:
            logging.debug("tasks_available before update: %s", pprint.pformat(self.tasks_available))
            for aid in aids:
                tids = aids[aid]
                self.tasks_available[aid] = {}
                for tid in tids:
                    task = tids[tid]
                    self.tasks_available[aid][task["id"]] = task
            logging.debug("tasks_available after update: %s", pprint.pformat(self.tasks_available))
            
                

        # Call split function to generate tasks from the assignment

        #print "TM: Breaking down assignment into tasks"
        #splits = self.newton.split_image(div=assignment["div"],
        #                        img_width=assignment["img_width"],
        #                        img_height=assignment["img_height"],
        #                        half_real_size=assignment["half_real_size"],
        #                        half_imag_size=assignment["half_imag_size"],
        #                        real_offset=assignment["real_offset"],
        #                        imag_offset=assignment["imag_offset"],
        #                        )

        ## Create a task for each split
        #for split in splits:
        #    self.tasks += [Task(description=split, aid=assignment_task.id)]

