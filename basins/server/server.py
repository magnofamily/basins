import base64

from SimpleXMLRPCServer import SimpleXMLRPCServer
from SimpleXMLRPCServer import SimpleXMLRPCRequestHandler
from basins.server.task_manager import TaskManager
from SocketServer import ThreadingMixIn
from time import sleep
import logging


logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',)
                    #datefmt='%m-%d %H:%M')
logger = logging.getLogger(__name__)

class Handlers:
    def __init__(self):
        self.tm = TaskManager()
        self.var = 0

    def request_task(self):
        logging.debug("Receiving a request for a new task")
        task = self.tm.request_task()
        return task

    def submit_resolved_task(self, task):
        logging.debug("Receiving a request for a task result submition")
        #task_result = base64.b64decode(encoded_task_result)
        self.tm.submit_resolved_task(task)
        return 0

    def add_tasks(self, tasks):
        #assignment = base64.b64decode(encoded_assignment)
        self.tm.add_tasks(tasks)
        return 0

    def rpc_test(self):
        print "var: ", self.var
        self.var += 1
        sleep(100)
        return None


# This will make SimpleXMLRPCServer multithreaded
class RPCThreading(ThreadingMixIn, SimpleXMLRPCServer):
        pass


class RequestHandler(SimpleXMLRPCRequestHandler):
        rpc_paths = ('/RPC2',)


def start_server(rpc_calls=Handlers()):
    #server = SimpleXMLRPCServer(("0.0.0.0", 8000),   # single threaded
    server = RPCThreading(("0.0.0.0", 8000),          # multi-threaded
                                requestHandler=RequestHandler,
                                allow_none=False)
    server.register_instance(rpc_calls)
    server.serve_forever()


if __name__ == "__main__":
    start_server(Handlers())
