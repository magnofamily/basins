from basins.server.server import start_server, RPCCalls
from uuid import uuid1


class MyRPCCalls(RPCCalls):
    def gen_task(self):
        return (str(uuid1()), "<some task>")

    def integrate_result(self, task_id, result):
        print "result:", task_id, result


start_server(MyRPCCalls())
