
import os
import pep8


def test_pep8_conformance():
    """Test that the project conforms to PEP8."""
    pep8style = pep8.StyleGuide(quiet=False)
    python_files = []
    # FIXME: a path relative to project base dir should be used instead
    # if this test file is moved to a different directory this won't work
    project_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                               "..")
    for root, _, files in os.walk(project_dir):
        python_files += \
            [os.path.join(root, s) for s in files if s.endswith(".py")]
    result = pep8style.check_files(python_files)
    assert result.total_errors == 0, "Found code style errors (and warnings)."
