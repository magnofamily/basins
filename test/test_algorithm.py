import pprint
import pytest
from basins.common.algorithm import Newton

@pytest.mark.parametrize("params,expected", [
    ({"img_width": 22, "img_height": 22,
      "half_real_size": 1.0, "half_imag_size": 1.0,
      "real_offset": 0.0, "imag_offset": 0.0},
     {"img_width": 22, "img_height": 22,
      "half_real_size": 1.0, "half_imag_size": 1.0,
      "real_offset": 0.0, "imag_offset": 0.0}),
    ({"img_width": 21, "img_height": 21,
      "half_real_size": 1.0, "half_imag_size": 1.0,
      "real_offset": 0.0, "imag_offset": 0.0},
     {"img_width": 21, "img_height": 21,
      "half_real_size": 1.0, "half_imag_size": 1.0,
      "real_offset": 0.0, "imag_offset": 0.0}),
])
def test_split_div1(request, params, expected):
    assert Newton().split_image(div=1, **params)[(0, 0)] == expected

@pytest.mark.parametrize("params,expected", [
    ({"img_width": 22, "img_height": 22,
      "half_real_size": 1.0, "half_imag_size": 1.0,
      "real_offset": 0.0, "imag_offset": 0.0},
     {(0, 0): { "img_width": 11, "img_height": 11,
               "half_real_size": 0.5, "half_imag_size": 0.5,
               "real_offset": -0.5, "imag_offset": 0.5},
      (0, 1): { "img_width": 11, "img_height": 11,
               "half_real_size": 0.5, "half_imag_size": 0.5,
               "real_offset": 0.5, "imag_offset": 0.5},
      (1, 0): { "img_width": 11, "img_height": 11,
               "half_real_size": 0.5, "half_imag_size": 0.5,
               "real_offset": -0.5, "imag_offset": -0.5},
      (1, 1): { "img_width": 11, "img_height": 11,
               "half_real_size": 0.5, "half_imag_size": 0.5,
               "real_offset": 0.5, "imag_offset": -0.5}}),
])
def test_split_div4(request, params, expected):
    assert Newton().split_image(div=4, **params) == expected



#     raw_image_data = newton(img_width=101, img_height=101,
#                             half_real_size=1.0, half_imag_size=1.0,
#                             real_offset=0.0, imag_offset=0.0,
#                             equ_coefs=[1, 0, 0, 0, 0, -1],
#                             max_iter_to_root=30)
#     assert len(raw_image_data) == 10201, "newton basins algorithm generating"
#     "wrong image data"
